#ifndef DATAINPUT_H
#define DATAINPUT_H
#include <iostream>

struct sInputData{
	int modelingTime;
	size_t firstDriveCapacity;
	size_t secondDriveCapacity;
	size_t firstGenTime;
	size_t firstGenTimeError;
	size_t secondGenTime;
	size_t secondGenTimeError;
	size_t sectionGenTime;
	size_t numOfTransFirstGen;
	size_t numOfTransSecondGen;
	sInputData(){
		modelingTime = 480;
		firstDriveCapacity = 10;
		secondDriveCapacity = 10;
		firstGenTime = 5;
		firstGenTimeError = 1;
		secondGenTime = 20;
		secondGenTimeError = 7;
		sectionGenTime = 10;
		numOfTransFirstGen = 5;
		numOfTransSecondGen = 20;
	}
};

class cDataInput{
	sInputData input;
public:
	void startFill();
	sInputData getData();
};
#endif // DATAINPUT_H
