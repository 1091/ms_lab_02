#ifndef DISPATCHER_H
#define DISPATCHER_H
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <time.h>
#include <sys/time.h>
#include "DataInput.h"

#define ACTIVE_EVENTS 4
#define TRANSACTS 100500
#define QUEUES 100500
#define SEARCH_INIT_MIN 100500

class Dispatcher{
	sInputData features;
	int systemTime, numberOfTransacts, numberOfTrFirst, numberOfTrSecond, processedSections, unprocessedSections;
	float missingProbability, queueAverageFirstDrive, queueAverageSecondDrive;
	int queueMaxFirstDrive, queueMaxSecondDrive, firstDrQNum, secondDrQNum;
	int S[2];
	int A[ACTIVE_EVENTS][4];
	int TR[TRANSACTS][7];
	int firstDriveQueue[QUEUES];
	int secondDriveQueue[QUEUES];
	struct timeval tv1,tv2,dtv;
	struct timezone tz;
public:
	Dispatcher(sInputData input);
	void startModeling();
	void calculateVariables();
	void printResults();
private:
	int findAMinTime();
	void doActivity(size_t);
	bool checkCondition(size_t);
	int PlusMinus(int);
	size_t amountOfBSigns();
	void generateTRtransacts(size_t, size_t);
	bool findProcessingTransacts();
	void addTRsignOfProcessing();
	void printTR();
	int numberOfServed();
	int numberOfNotServed();
	void time_start();
	long time_stop();
	enum activityOfActiveEvents{
		GOS1,
		GOS2,
		GOS3,
		GOS4
	};
	enum AIndexes{
		A_EVENT_NUMBER,
		A_TRANSACT_NUMBER,
		A_TIME_OF_EVENT_START,
		A_GOS
	};
	enum TRIndexes{
		TR_TRANSACT_NUMBER,
		TR_GENERATION_TIME,
		TR_START_OF_SERVICE,
		TR_SIGN_OF_PROCESSING,
		TR_SIGN_OF_PROCESSED,
		TR_TYPE
	};
	enum TRtypes{
		TR_TYPE_FIRST,
		TR_TYPE_SECOND
	};
	enum Xi{
		X1,
		X2,
		X3,
		X4
	};
	enum Ei{
		E1
	};
	enum EiCondition{
		E1_COND
	};
	enum Si{
		S_FIRST_DRIVE,
		S_SECOND_DRIVE
	};
};

#endif // DISPATCHER_H

